
const firstName = prompt('Введіть ім\'я:');
const lastName = prompt('Введіть прізвище:');
const birthdayStr = prompt('Введіть дату народження у форматі dd.mm.yyyy:');

function createNewUser(firstName, lastName, birthday) {
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday, 
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },



        getAge: function () {
            const birthYear = new Date(this.birthday).getFullYear(); 
            const currentYear = new Date().getFullYear();
            return currentYear - birthYear;
        },



        getPassword: function () {
            const firstLetterUpper = this.firstName[0].toUpperCase();
            const lastNameLower = this.lastName.toLowerCase();
            const birthYearStr = String(new Date(this.birthday).getFullYear()); 
            return (`${firstLetterUpper}${lastNameLower}${birthYearStr}`);
        }
    };
    return newUser;
}

const newUser = createNewUser(firstName, lastName, birthdayStr); 
const login = newUser.getLogin();
const age = newUser.getAge();
const password = newUser.getPassword();
console.log(login);
console.log(`Рік народження: ${newUser.birthday}`);
console.log(`Вік користувача: ${age} років`);
console.log(`Пароль: ${password}`);

